import { PostMoreButton } from "./buttons/post-more-button";

type TPostProps = {
  posts: any[];
};

export const PostList = (props: TPostProps) => {
  return (
    <>
      <ol>
        {props.posts.map((item) => (
          <PostItem post={item} />
        ))}
      </ol>
    </>
  );
};

type TPostItemProps = {
  post: {
    id: number;
    authorId: number;
    authorName: string;
    title: string;
    text: string;
    status: string;
    createdAt: string;
  };
};
const PostItem = (props: TPostItemProps) => {
  const date: string = props.post.createdAt.slice(0, 10);
  return (
    <>
      <div className="every_post_table">
        <div>
          <strong className="post">title:</strong>
          <strong className="post_title_content">{props.post.title}</strong>
        </div>
        <div>
          <strong className="post">post:</strong>
          <strong className="post_text_content">{props.post.text}</strong>
        </div>
        <div className="post_author">
          <strong className="post">author:</strong>
          <strong className="post_author_content">
            {props.post.authorName}
            <div className="post_date_content">{date}</div>
          </strong>
        </div>
        <PostMoreButton id={props.post.id} />
      </div>
    </>
  );
};
