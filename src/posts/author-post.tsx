import { DraftedPost } from "./drafted-post";
import { PostedPost } from "./posted-post";

export type TPost = {
  id: number;
  authorId: number;
  authorName: string;
  title: string;
  text: string;
  status: string;
  createdAt: string;
};

export type TpropsAuthorPost = {
  post: TPost;
};

export const PostAuthorItem = (props: TpropsAuthorPost) => {
  return (
    <>
      {props.post.status === "draft" ? (
        <DraftedPost post={props.post} />
      ) : (
        <PostedPost post={props.post} />
      )}
    </>
  );
};
