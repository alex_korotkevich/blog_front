import { useNavigate } from "react-router-dom";
import { Comments } from "../comments/comments";
import { Logout } from "../common/logout";
import { postController } from "../http/post-controller";
import { PostTags } from "../tags/tags";
import { TpropsAuthorPost } from "./author-post";
import { ReturnButton } from "./buttons/return-button";
import { ReturnToMyPostsButton } from "./buttons/return-to-my-posts-button";

export const PostedPost = (props: TpropsAuthorPost) => {
  const navigate = useNavigate();
  const date: string = props.post.createdAt.slice(0, 10);

  const toDraft = async (id: number) => {
    const status = "draft";
    await postController.publish(id, status);
    navigate(`/post/my`);
  };

  const remove = async (id: number) => {
    const status = "deleted";
    await postController.publish(id, status);
    navigate(`/post/my`);
  };

  return (
    <>
      <div>
        <Logout />
      </div>
      <div>
        <ReturnToMyPostsButton />
      </div>
      <div>
        <ReturnButton />
      </div>
      <div>
        <button
          className="buttonToDraft"
          onClick={() => toDraft(props.post.id)}
        >
          <strong> To draft</strong>
        </button>
      </div>
      <div>
        <button
          className="buttonToDelete"
          onClick={() => remove(props.post.id)}
        >
          <strong> Delete</strong>
        </button>
      </div>
      <div className="open_post_table">
        <div>
          <strong className="post">title:</strong>
          <strong className="post_title_content">{props.post.title}</strong>
        </div>
        <div>
          <strong className="post">post:</strong>
          <strong className="post_text_content">{props.post.text}</strong>
        </div>
        <div className="post_author">
          <strong className="post">author:</strong>
          <strong className="post_author_content">
            {props.post.authorName}
            <div className="post_date_content">{date}</div>
          </strong>
        </div>
        <PostTags />
        <Comments />
      </div>
    </>
  );
};
