import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { postController } from "../http/post-controller";
import { ReturnToOpenPostButton } from "./buttons/return-to-open-post";

type TPost = {
  id: number;
  userId: number;
  authorName: string;
  title: string;
  text: string;
  status: string;
};

export const UpdatePost = () => {
  const [post, setPost] = useState<TPost>({
    id: 0,
    userId: 0,
    authorName: "",
    title: "",
    text: "",
    status: "",
  });

  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const params: any = useParams();
  const navigate = useNavigate();

  const loadPost = async () => {
    const post = await postController.findOne(params.id);
    setPost(post);
  };

  useEffect(() => {
    loadPost();
  }, []);

  const update = async () => {
    await postController.update(post.id, { text, title });
    navigate(`/post/${params.id}`);
  };
  return (
    <>
      <ReturnToOpenPostButton />
      <div className="open_post_table">
        <strong className="post">title:</strong>
        <input
          className="input_login_password"
          value={title}
          onChange={(event) => setTitle(event.target.value)}
        ></input>
      </div>
      <div className="open_post_table">
        <strong className="post">post: </strong>
        <input
          className="input_login_password"
          value={text}
          onChange={(event) => setText(event.target.value)}
        ></input>
      </div>
      <button className="buttonUpdate" onClick={update}>
        <strong> update</strong>{" "}
      </button>
    </>
  );
};
