import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { postController } from "../http/post-controller";
import { CreateTags } from "../tags/tags";
import { ReturnToMyPostsButton } from "./buttons/return-to-my-posts-button";

export const CreatePost = () => {
  const navigate = useNavigate();

  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const [color, setColor] = useState("");
  const [tags, setTags] = useState<number[]>([]);

  const addTag = async (id: number) => {
    tags.push(id);
    setTags(tags);
  };

  const removeTag = async (id: number) => {
    const newTags = tags.filter((item) => item !== id);
    setTags(newTags);
  };

  const savePost = async () => {
    try {
      if (!title || !text) {
        return setColor("red");
      }
      setColor("");
      const id = await postController.save({ title, text, tags });
      navigate(`/post/${id}`);
    } catch (err) {
      setColor("red");
    }
  };

  return (
    <>
      <ReturnToMyPostsButton />
      <div className="open_post_table">
        <label>
          <strong className="post">title: </strong>
          <input
            className="input_login_password"
            value={title}
            onChange={(event) => setTitle(event.target.value)}
          ></input>
        </label>
      </div>

      <div className="open_post_table">
        <label>
          <strong className="post">post: </strong>
          <input
            className="input_login_password"
            value={text}
            onChange={(event) => setText(event.target.value)}
          ></input>
        </label>
      </div>

      <CreateTags addTag={addTag} removeTag={removeTag} />
      <button
        className="buttonPost"
        onClick={savePost}
        style={{ background: color }}
      >
        <strong>
          <span>save</span>
        </strong>
      </button>
    </>
  );
};
