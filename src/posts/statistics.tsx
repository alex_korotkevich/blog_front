import { useEffect, useState } from "react";
import { postController } from "../http/post-controller";
import { ReturnToMyPostsButton } from "./buttons/return-to-my-posts-button";

type TStat = {
  posts: number;
  draft: number;
  comments: number;
};
export const Statistics = () => {
  const [post, setPost] = useState<TStat>({
    posts: 0,
    draft: 0,
    comments: 0,
  });

  const statistics = async () => {
    const data = await postController.statistics();
    setPost(data);
  };
  useEffect(() => {
    statistics();
  }, []);

  return (
    <>
      <ReturnToMyPostsButton />
      <strong>
        <ol>
          <div className="dash">
            <span className="post">my posts: </span>
            {post.posts || 0}
          </div>

          <div className="dash">
            <span className="post">my drafts:</span> {post.draft || 0}
          </div>

          <div className="dash">
            <span className="post">number of comments:</span>
            {post.comments || 0}
          </div>
        </ol>
      </strong>
    </>
  );
};
