import { useState } from "react";

type TPaginationProps = {
  count: number;
  load: Function;
};

export const Pagination = (props: TPaginationProps) => {
  const take = 2;
  const maxPage = Math.ceil(props.count / take);
  const [page, setPage] = useState(1);

  const load = async (page: number) => {
    props.load(take, page);
  };

  const prev = () => {
    if (page > 1) {
      _setPage(page - 1);
    }
  };

  const next = () => {
    if (page < maxPage) {
      _setPage(page + 1);
    }
  };

  const _setPage = async (curPage: number) => {
    setPage(curPage);
    await load(curPage);
  };

  return (
    <div className="paginationButtonsBlock">
      <button className="buttonPage" onClick={prev}>
        prev
      </button>
      {[...Array(maxPage)].map((_, i) => (
        <button className="buttonPage" onClick={() => _setPage(i + 1)}>
          {i + 1}
        </button>
      ))}
      <button className="buttonPage" onClick={next}>
        next
      </button>
    </div>
  );
};
