import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../App";

export const GoToMyPostsButton = () => {
  const navigate = useNavigate();
  const user = useContext<any>(UserContext);
  const more = () => {
    navigate(`/post/my`);
  };

  return (
    <button className="buttonMyPosts" onClick={more}>
      <strong>
        <span>{user?.name?.[0] || ""}</span>
      </strong>
    </button>
  );
};
