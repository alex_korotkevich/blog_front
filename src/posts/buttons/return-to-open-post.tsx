import { useNavigate, useParams } from "react-router-dom";

export const ReturnToOpenPostButton = () => {
  const navigate = useNavigate();
  const params: any = useParams();

  const returnButton = () => {
    navigate(`/post/${params.id}`);
  };

  return (
    <button className="buttonReturn" onClick={returnButton}>
      <strong>
        <span>return</span>
      </strong>
    </button>
  );
};
