import { useNavigate } from "react-router-dom";

type TCommentsProps = {
  id: number;
};

export const SeeCommentsButton = (props: TCommentsProps) => {
  const navigate = useNavigate();

  const seeComments = () => {
    navigate(`/post/${props.id}/comments`);
  };

  return (
    <button onClick={seeComments}>
      <strong>
        <span>see comments</span>
      </strong>
    </button>
  );
};
