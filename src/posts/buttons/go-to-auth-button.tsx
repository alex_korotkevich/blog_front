import { useNavigate } from "react-router-dom";

export const GoToAuthButton = () => {
  const navigate = useNavigate();

  const more = () => {
    navigate(`/auth`);
  };

  return (
    <button className="buttonGoToAuth" onClick={more}>
      <strong>
        <span>To auth</span>
      </strong>
    </button>
  );
};
