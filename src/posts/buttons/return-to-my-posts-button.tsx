import { useNavigate } from "react-router-dom";

export const ReturnToMyPostsButton = () => {
  const navigate = useNavigate();

  const returnButton = () => {
    navigate(`/post/my`);
  };

  return (
    <button className="buttonReturn" onClick={returnButton}>
      <strong>
        <span>my posts</span>
      </strong>
    </button>
  );
};
