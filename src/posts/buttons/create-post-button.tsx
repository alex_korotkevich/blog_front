import { useNavigate } from "react-router-dom";

export const GoToCreatePostButton = () => {
  const navigate = useNavigate();

  const create = () => {
    navigate(`/post/create`);
  };

  return (
    <button className="buttonCreateNewPost" onClick={create}>
      <strong>
        <span>Create new post</span>
      </strong>
    </button>
  );
};
