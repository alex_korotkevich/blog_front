import { ChangeEvent } from "react";

type TFileUploadButtonProps = {
  name: string;
  upload: (file: FormData) => Promise<void>;
};
export const FileUploadButton = (props: TFileUploadButtonProps) => {
  const _upload = async (e: ChangeEvent<HTMLInputElement>) => {
    try {
      const files = e.target.files;
      if (files && files.length > 0) {
        const file = files[0];
        const data = new FormData();
        data.append("file", file);
        await props.upload(data);
      }
      e.target.value = "";
    } catch (e) {
      throw e;
    }
  };
  return (
    <input
      className="buttonPage"
      name={props.name}
      onChange={_upload}
      type="file"
    />
  );
};
