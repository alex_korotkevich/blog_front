import { useNavigate } from "react-router-dom";

type TMoreProps = {
  id: number;
};

export const PostMoreButton = (props: TMoreProps) => {
  const navigate = useNavigate();

  const more = () => {
    navigate(`/post/${props.id}`);
  };

  return (
    <button className="buttonMore" onClick={more}>
      <strong>
        <span>More</span>
      </strong>
    </button>
  );
};
