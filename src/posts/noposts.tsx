export const NoPosts = () => {
  return (
    <div className="no_posts_table">
      <strong className="no_posts_text">There are no posts yet</strong>
    </div>
  );
};
