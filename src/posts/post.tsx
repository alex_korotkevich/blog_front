import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { UserContext } from "../App";
import { Comments } from "../comments/comments";
import { postController } from "../http/post-controller";
import { PostTags } from "../tags/tags";
import { PostAuthorItem } from "./author-post";
import { ReturnButton } from "./buttons/return-button";

type TPost = {
  id: number;
  authorId: number;
  authorName: string;
  title: string;
  text: string;
  status: string;
  createdAt: string;
};
type TTokenProps = {
  token: boolean;
};

export const OpenPost = (props: TTokenProps) => {
  const [post, setPost] = useState<TPost>({
    id: 0,
    authorId: 0,
    authorName: "",
    title: "",
    text: "",
    status: "",
    createdAt: "",
  });

  const params: any = useParams();
  const user = useContext<any>(UserContext);

  const loadPost = async () => {
    const post = await postController.findOne(params.id);
    setPost(post);
  };

  useEffect(() => {
    loadPost();
  }, []);

  return (
    <>
      {props.token && post.authorId === user?.id ? (
        <PostAuthorItem post={post} />
      ) : (
        <PostItem post={post} />
      )}
    </>
  );
};
type TPostItemProps = {
  post: TPost;
};

const PostItem = (props: TPostItemProps) => {
  const date: string = props.post.createdAt.slice(0, 10);

  return (
    <>
      <ReturnButton />
      <div className="open_post_table">
        <div>
          <strong className="post">title:</strong>
          <strong className="post_title_content">{props.post.title}</strong>
        </div>
        <div>
          <strong className="post">post:</strong>
          <strong className="post_text_content">{props.post.text}</strong>
        </div>
        <div className="post_author">
          <strong className="post">author:</strong>
          <strong className="post_author_content">
            {props.post.authorName}
            <div className="post_date_content">{date}</div>
          </strong>
        </div>
        <PostTags />
        <Comments />
      </div>
    </>
  );
};
