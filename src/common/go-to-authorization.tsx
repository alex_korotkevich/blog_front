import { useNavigate } from "react-router-dom";

export const GoToAuthorization = () => {
  const navigate = useNavigate();

  const authorization = () => {
    navigate(`/auth`);
  };

  return (
    <>
      <button className="buttonGoToAuthorization" onClick={authorization}>
        <strong>
          <span>Go To Authorization</span>
        </strong>
      </button>
    </>
  );
};
