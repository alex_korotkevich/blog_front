import { GoToMyPostsButton } from "../posts/buttons/go-to-my-posts-button";
import { GoToAuthorization } from "./go-to-authorization";
import { Logout } from "./logout";

type TAuthProps = {
  token: boolean;
};

export const Header = (props: TAuthProps) => {
  return (
    <>
      {" "}
      {props.token ? (
        <>
          <div>
            <Logout />
          </div>
          <div>
            <GoToMyPostsButton />
          </div>
        </>
      ) : (
        <GoToAuthorization />
      )}
    </>
  );
};
