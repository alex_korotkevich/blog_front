import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { tagController } from "../http/tag-controller";

type TTag = {
  id: number;
  postId: number;
  text: string;
};

type TTagItem = {
  tag: TTag;
  action?: (id: number) => void;
};

type TTagProps = {
  addTag: (id: number) => void;
  removeTag: (id: number) => void;
};

export const PostTags = () => {
  const params: any = useParams();
  const [state, setState] = useState<TTag[]>([]);

  const seeTags = async () => {
    const tags = await tagController.list(params.id);
    setState(tags);
  };

  useEffect(() => {
    seeTags();
  }, []);
  return (
    <>
      <div className="comments_content">
        <strong className="comments"> Tags:</strong>
        <ul>
          {state.map((item) => (
            <li>
              <OpenTagsItem tag={item} />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

const OpenTagsItem = (props: TTagItem) => {
  return <div>#{props.tag.text}</div>;
};

export const CreateTags = (props: TTagProps) => {
  const [state, setState] = useState<TTag[]>([]);
  const [allTags, setAllTags] = useState<TTag[]>([]);

  const removeTag = (id: number) => {
    const tag: TTag = state.find((item) => item.id === id)!;
    const newState = state.filter((item) => item.id !== id);
    setState(newState);
    allTags.push(tag);
    setAllTags(allTags);
    props.removeTag(tag.id);
  };

  const addTag = (id: number) => {
    const tag: TTag = allTags.find((item) => item.id === id)!;
    const newState = allTags.filter((item) => item.id !== id);
    setAllTags(newState);
    state.push(tag);
    setState(state);
    props.addTag(tag.id);
  };

  const load = async () => {
    const tags = await tagController.excludeList();
    setAllTags(tags);
  };

  useEffect(() => {
    load();
  }, []);
  return (
    <>
      <div className="comments_content">
        <strong className="comments"> Tags:</strong>

        <ul>
          {state.map((item) => (
            <li>
              <TagRemoveButton tag={item} action={removeTag} />
            </li>
          ))}
        </ul>
      </div>
      {allTags.map((item) => (
        <TagAddButton action={addTag} tag={item} />
      ))}
    </>
  );
};
const TagRemoveButton = (props: TTagItem) => {
  return (
    <button onClick={() => props.action!(props.tag.id)}>
      #{props.tag.text}
    </button>
  );
};

const TagAddButton = (props: TTagItem) => {
  return (
    <button onClick={() => props.action!(props.tag.id)}>
      #{props.tag.text}
    </button>
  );
};
