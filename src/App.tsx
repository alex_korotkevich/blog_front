import React, { createContext, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { TState } from ".";
import { authController } from "./http/auth-controller";
import { AuthPage } from "./page/auth-page";
import { MainPage } from "./page/main-page";
import { MyPostListPage } from "./page/my-post-page";
import { RegistrationPage } from "./page/registration-page";
import { CreatePost } from "./posts/create-post";
import { OpenPost } from "./posts/post";
import { UpdatePost } from "./posts/post-update";
import { Statistics } from "./posts/statistics";

export const UserContext = createContext(null);

export const App = () => {
  const token = useSelector<TState, boolean>((state) => !!state.token);

  const [user, setUser] = useState<any | null>(null);

  const load = async () => {
    const userData = await authController.me();
    setUser(userData);
  };

  useEffect(() => {
    load();
  }, []);

  return (
    <>
      <UserContext.Provider value={user}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<MainPage token={token} />}></Route>
            <Route
              path="/auth"
              element={<AuthPage setUser={setUser} />}
            ></Route>
            <Route path="/registration" element={<RegistrationPage />}></Route>
            <Route path="/post/my" element={<MyPostListPage />}></Route>
            <Route path="/post/create" element={<CreatePost />}></Route>
            <Route
              path="/post/:id"
              element={<OpenPost token={token} />}
            ></Route>
            <Route path="/post/:id/update" element={<UpdatePost />}></Route>
            <Route path="/post/statistics" element={<Statistics />}></Route>
          </Routes>
        </BrowserRouter>
      </UserContext.Provider>
    </>
  );
};
