import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { commentController } from "../http/comment-controller";

type TComments = {
  id: number;
  postId: number;
  userId: number;
  text: string;
  createdAt: string;
};

export const Comments = () => {
  const params: any = useParams();
  const [state, setState] = useState<TComments[]>([]);
  const [text, setText] = useState("");

  const addComment = (comment: any) => {
    setState([...state, comment]);
  };

  const createComment = async () => {
    const newComment = await commentController.create(params.id, text);
    addComment(newComment);
    setText("");
  };

  const seeComments = async () => {
    const comments = await commentController.list(params.id);
    setState(comments);
  };
  useEffect(() => {
    seeComments();
  }, []);

  return (
    <>
      <div className="comments_content">
        <strong className="comments"> Comments:</strong>

        <ul>
          {state.map((item) => (
            <li>
              <CommentItem comment={item} />
            </li>
          ))}
        </ul>
      </div>
      <div className="comment_table">
        <label>
          <strong className="comment">comment: </strong>
          <input
            className="input"
            value={text}
            onChange={(event) => setText(event.target.value)}
          ></input>
        </label>
        <button className="buttonComment" onClick={createComment}>
          comment
        </button>
      </div>
    </>
  );
};

type TCommentItem = {
  comment: {
    postId: number;
    userId?: number;
    text: string;
    authorName?: string;
    createdAt: string;
  };
};

const CommentItem = (props: TCommentItem) => {
  const date: string = props.comment.createdAt.slice(0, 10);

  return (
    <>
      <div>{props.comment.text}</div>
      <strong>by:</strong>
      {props.comment.authorName || "anon"}
      <div className="post_date_content">{date}</div>
    </>
  );
};
