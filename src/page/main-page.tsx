import { useEffect, useState } from "react";
import { Header } from "../common/header";
import { postController } from "../http/post-controller";
import { NoPosts } from "../posts/noposts";
import { Pagination } from "../posts/pagination";
import { PostList } from "../posts/post-list";

type TAuthProps = {
  token: boolean;
};

export const MainPage = (props: TAuthProps) => {
  const [state, setState] = useState<any[]>([]);
  const [count, setCount] = useState<number>(0);

  const load = async (take: number = 2, page: number = 1) => {
    const data = await postController.list({ take, page });
    setState(data.posts);
    setCount(data.count);
  };

  useEffect(() => {
    load();
  }, []);
  return (
    <>
      <Header token={props.token} />
      {state.length ? <PostList posts={state} /> : <NoPosts />}
      <Pagination load={load} count={count} />
    </>
  );
};
