import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { ActionType } from "../action-type.enum";
import { authController } from "../http/auth-controller";
import { ReturnButton } from "../posts/buttons/return-button";

type TLoginProps = {
  setUser: Function;
};

export const AuthPage = (props: TLoginProps) => {
  const dispatch = useDispatch();

  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [color, setColor] = useState("");

  const navigate = useNavigate();

  const registration = async () => {
    navigate(`/registration`);
  };

  const loginButton = async () => {
    try {
      if (!login || !password) {
        return setColor("red");
      }
      setColor("");
      const token = await authController.login(login, password);
      dispatch({ type: ActionType.setToken, payload: token });
      const me = await authController.me();
      props.setUser(me);
      navigate(`/`);
    } catch (err) {
      setColor("red");
    }
  };

  return (
    <>
      <ReturnButton />
      <div className="login_table">
        <label>
          <strong className="login_password_text">LOGIN: </strong>
          <input
            className="input_login_password"
            value={login}
            onChange={(event) => setLogin(event.target.value)}
          ></input>
        </label>
      </div>

      <div className="password_table">
        <label>
          <strong className="login_password_text">PASSWORD: </strong>
          <input
            className="input_login_password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          ></input>
        </label>
      </div>

      <div>
        <button
          className="buttonLogin"
          onClick={loginButton}
          style={{ background: color }}
        >
          <strong>
            <span> Login</span>
          </strong>
        </button>
      </div>

      <button className="buttonGoToRegistration" onClick={registration}>
        <strong>
          <span>Go To Registration</span>
        </strong>
      </button>
    </>
  );
};
