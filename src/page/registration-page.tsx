import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { authController } from "../http/auth-controller";
import { GoToAuthButton } from "../posts/buttons/go-to-auth-button";
import { ReturnButton } from "../posts/buttons/return-button";

export const RegistrationPage = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const navigate = useNavigate();

  const userRegistration = async () => {
    if (!login || !password || !name || !email) {
      return;
    }
    await authController.registration(login, password, name, email);
    navigate(`/auth`);
  };

  return (
    <>
      <div>
        {" "}
        <GoToAuthButton />
      </div>{" "}
      <ReturnButton />
      <div>
        {" "}
        <div className="login_table">
          <label>
            <strong className="login_password_text">LOGIN:</strong>
            <input
              className="input_login_password"
              value={login}
              onChange={(event) => setLogin(event.target.value)}
            ></input>
          </label>
        </div>
        <div className="password_table">
          <label>
            <strong className="login_password_text"> PASSWORD:</strong>
            <input
              className="input_login_password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            ></input>
          </label>
        </div>
      </div>
      <div className="name_table">
        <label>
          <strong className="login_password_text"> NAME:</strong>
          <input
            className="input_login_password"
            value={name}
            onChange={(event) => setName(event.target.value)}
          ></input>
        </label>
      </div>
      <div className="login_table">
        <label>
          <strong className="login_password_text"> EMAIL:</strong>
          <input
            className="input_login_password"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          ></input>
        </label>
      </div>
      <div>
        <button className="buttonRegistration" onClick={userRegistration}>
          <strong>
            {" "}
            <span>Registration</span>
          </strong>{" "}
        </button>
      </div>
    </>
  );
};
