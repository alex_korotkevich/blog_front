import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../App";
import { Logout } from "../common/logout";
import { authController } from "../http/auth-controller";
import { postController } from "../http/post-controller";
import { GoToCreatePostButton } from "../posts/buttons/create-post-button";
import { ReturnButton } from "../posts/buttons/return-button";
import { FileUploadButton } from "../posts/buttons/upload-button";
import { NoPosts } from "../posts/noposts";
import { Pagination } from "../posts/pagination";
import { PostList } from "../posts/post-list";

export const MyPostListPage = () => {
  const user = useContext<any>(UserContext);

  const [count, setCount] = useState<number>(0);
  const [status, setStatus] = useState<string | undefined>(undefined);

  const navigate = useNavigate();
  const [state, setState] = useState<any[]>([]);

  const load = async (take: number = 2, page: number = 1) => {
    const data = await postController.myList({ status, take, page });
    setState(data.posts);
    setCount(data.count);
  };

  useEffect(() => {
    load();
  }, [status]);

  const upload = async (data: FormData) => {
    await authController.upload(data);
  };

  const statistics = async () => {
    navigate(`/post/statistics`);
  };

  return (
    <>
      <h3>
        Hi, <strong className="post_title_content"> {user?.name || ""}</strong>!
      </h3>
      <div>
        <Logout />
      </div>
      <div>
        <ReturnButton />
      </div>
      <div>
        <img
          className="img"
          src={`/uploads/${user?.avatar}`}
          alt="avatar"
          width={200}
          height={200}
        />
      </div>
      <div>
        {" "}
        <FileUploadButton name={"avatar"} upload={upload} />
      </div>
      <div>
        {" "}
        <div>
          <button className="button_all" onClick={() => setStatus(undefined)}>
            <strong> all</strong>
          </button>
        </div>
        <div>
          <button className="button_draft" onClick={() => setStatus("draft")}>
            <strong> draft</strong>
          </button>
        </div>
        <div>
          <button className="button_posted" onClick={() => setStatus("posted")}>
            <strong> posted</strong>
          </button>
          <div>
            <button className="buttonStatistics" onClick={statistics}>
              <strong>
                {" "}
                <span>Statistics</span>
              </strong>
            </button>
          </div>
        </div>
        <GoToCreatePostButton />
      </div>
      {state.length ? <PostList posts={state} /> : <NoPosts />}
      <Pagination load={load} count={count} />
    </>
  );
};
