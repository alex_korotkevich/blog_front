import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/post`;

class CommentController {
  list = async (id: number) => {
    try {
      const res = await axios.get(`${baseUrl}/${id}/comments`);
      return res.data;
    } catch (err) {
      throw err;
    }
  };

  create = async (id: number, text: string) => {
    try {
      const token = localStorage.getItem("token");

      const res = await axios.post(
        `${baseUrl}/${id}/create`,
        { text },
        {
          headers: { authorization: token || "" },
        }
      );
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };
}

export const commentController = new CommentController();
