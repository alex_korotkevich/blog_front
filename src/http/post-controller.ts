import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/post`;
type TListProps = {
  status?: string;
  take?: number;
  page?: number;
};

class PostController {
  list = async (data: TListProps) => {
    try {
      const res = await axios.get(baseUrl, {
        params: data,
      });
      return res.data;
    } catch (err) {
      throw err;
    }
  };

  myList = async (data: TListProps) => {
    try {
      const token = localStorage.getItem("token");
      const res = await axios.get(`${baseUrl}/myposts`, {
        headers: { authorization: token || "" },
        params: data,
      });
      return res.data;
    } catch (err) {
      localStorage.removeItem("token");
      throw err;
    }
  };

  findOne = async (id: number) => {
    const res = await axios.get(`${baseUrl}/${id}`);
    return res.data;
  };

  save = async (data: { title: string; text: string; tags: number[] }) => {
    // save to draft
    try {
      const token = localStorage.getItem("token");
      const res = await axios.post(baseUrl, data, {
        headers: { authorization: token || "" },
      });
      return res.data.id;
    } catch (err) {
      console.log(err);
    }
  };

  publish = async (id: number, status: string) => {
    // from draft to post
    try {
      const token = localStorage.getItem("token");
      await axios.put(
        `${baseUrl}/${id}/publish`,
        { status },
        {
          headers: { authorization: token || "" },
        }
      );
    } catch (err) {
      console.log(err);
    }
  };

  update = async (id: number, data: { title: string; text: string }) => {
    try {
      const token = localStorage.getItem("token");
      await axios.put(`${baseUrl}/${id}`, data, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      console.log(err);
    }
  };

  statistics = async () => {
    try {
      const token = localStorage.getItem("token");
      const res = await axios.get(`${baseUrl}/myposts/statistics`, {
        headers: { authorization: token || "" },
      });
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };
}

export const postController = new PostController();
