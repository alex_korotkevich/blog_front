import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}`;

class AuthController {
  login = async (login: string, password: string) => {
    try {
      const res = await axios.post(`${baseUrl}/login`, { login, password });
      return res.data;
    } catch (err) {
      localStorage.removeItem("token");
      throw err;
    }
  };

  registration = async (
    login: string,
    password: string,
    name: string,
    email: string
  ) => {
    await axios.post(`${baseUrl}/registration`, {
      login,
      password,
      name,
      email,
    });
  };

  logout = async () => {
    try {
      const token = localStorage.getItem("token");
      await axios.get(`${baseUrl}/logout`, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      localStorage.removeItem("token");
    }
  };
  me = async () => {
    try {
      const token = localStorage.getItem("token");
      const res = await axios.get(`${baseUrl}/me`, {
        headers: { authorization: token || "" },
      });
      return res.data;
    } catch (err) {
      localStorage.removeItem("token");
    }
  };

  upload = async (req: FormData) => {
    try {
      const token = localStorage.getItem("token");
      await axios.put(`${baseUrl}/upload`, req, {
        headers: { authorization: token || "" },
      });
    } catch (err) {
      localStorage.removeItem("token");
    }
  };
}

export const authController = new AuthController();
