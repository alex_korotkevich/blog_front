import axios from "axios";
const baseUrl = `${process.env.REACT_APP_BASE_URL}/tag`;

class TagController {
  list = async (id: number) => {
    try {
      const res = await axios.get(`${baseUrl}/${id}`);
      return res.data;
    } catch (err) {
      throw err;
    }
  };

  excludeList = async (postId?: number) => {
    try {
      const res = await axios.get(`${baseUrl}/exclude`, {
        params: { postId },
      });
      return res.data;
    } catch (err) {
      throw err;
    }
  };
}

export const tagController = new TagController();
